<h2>Friday,13 PyCall.jl</h2>
<p>
у модулі PyCall є досить неочікувана фіча - можна не тільки імпортувпти модулі Python але також безпосередньо вставляти код Python 
практично inline у  модуль на julia , а потім це викликати з julia.
</p>
<hr>
для повного розуміння демо бажано ознайомитись з  
<a href="https://github.com/JuliaPy/PyCall.jl">поясненням по PyCall.jl на github</a> а також згадати про те , що у python існує 
стандартна функція <a href="https://www.programiz.com/python-programming/methods/built-in/exec">exec</a>. Тому можна це усе поєднати 
у <b>python programmer friday-13 nightmare приблизно ось так:</b> ( пишемо модуль MyPython.jl та ... )
<hr>
<pre>
module MyPython
  using PyCall
  export hell
  function __init__()
    py"""
    def xec(x):
      exec(x)    
    """
   end
   hell(x) = py"xec"(x) 
end
</pre>
<p>
 а згадуємо про опис global для змінних у Python. 
</p>
<pre>
include("./MyPython.jl")
using .MyPython
hell("global a\na = 666")
hell("print(a)")
</pre>
<p>
там глобальна для python змінна a зберігає своє значення між викликами з julia. 
</p>
