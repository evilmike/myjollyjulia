#
# https://github.com/JuliaPy/PyCall.jl
#
module MyPython
  using PyCall
  export hell
  function __init__()
    py"""
    def xec(x):
      exec(x)    
    """
   end
   hell(x) = py"xec"(x) 
end
