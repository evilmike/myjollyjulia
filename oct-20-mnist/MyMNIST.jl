module MyMNIST
  using PyCall,Printf
  export mnistLoad
  function mnistLoad() 
    tf = pyimport("tensorflow")
    tfds = pyimport("tensorflow_datasets")
    ds = tfds.load("mnist"; split = "train", data_dir = "./datasets/mnist", batch_size = -1, download = false)
    ii = ds["image"]
    i = 0; 
    for p in ii 
      @printf("\n%d:",length(p))
      for k in p 
        @printf("\n%d ",length(k))
        for g in k
          tt = g[1].numpy()
          v = pyrepr(tt)
          h = parse(UInt8,v)
          @printf(" %02X",h) 
        end
      end; 
      i += 1
      if i==28; break; end #if 
    end # for p
    @printf("\n\nThat's all!\n");
  end  # mnistLoad
end # MyMNIST
using .MyMNIST
mnistLoad()
#
#

