<h2>
MNIST uncovered:
</h2>
<p>
<a href="https://learn.microsoft.com/en-us/azure/open-datasets/dataset-mnist?tabs=azureml-opendatasets">
MNIST на learn.microsoft.com
</a>
Але ми робимо усе засобами tensorflow 2.14.0 на julia з використанням модуля PyCall.
закачати дадасет mnist можна ось так:
</p>
<pre>
ds = tfds.load("mnist"; split = "train", data_dir = "./datasets/mnist", batch_size = -1, download = true)
</pre>
<p>
Датасет mnist засобами tfds ( tensorflow_datasets ) - тренування моделей для розпізнавання цифр.
</p>
<p>
Ми його закачали засобами tfds та витягнули з ньго дані.
має запускатися ось так:
</p>
<pre>
source ./myconsole.sh ./MyMNIST.jl >./o-mnist.txt
</pre>
