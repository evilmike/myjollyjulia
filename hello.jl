function aa() 
  i = 0
  while true 
    println("1 $i")
    i = i + 1
    yield()
  end
end

function bb() 
  j = 0
  while true
    println("2 $j")
    j = j + 1
    yield()
  end
end

b = Task(aa)
schedule(b)
c = Task(bb)
schedule(c)
while true 
  yield()
end
#



