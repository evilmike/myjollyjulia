module My
  export hi, hint
  function hint(args::Vector{Int})::Int
    s = 0
    for t in args
      s = s + t
    end
    return s
  end
  function hi(args::Vector{String})
    for t in args
      println("$t")
    end
  end
end

