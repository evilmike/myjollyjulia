module FuDB
 
using SQLite,DBInterface,Printf

function my()
  cc = DBInterface.connect(SQLite.DB,"./allo.db")
  res = DBInterface.execute(cc,"SELECT isn,xname,info FROM dic")
  for r in res 
    @printf("%d\t%s\t%s\n",r[1],r[2],r[3])
  end
  DBInterface.close(cc)
end

function julia_main()::Cint 
  my()
  return 0
end

end # module FuDB
