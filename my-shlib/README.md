<h2>Наше Кіно для StaticCompiler.jl</h2>
<p>
це ми трохи познущаємося над статичною компіляцією для julia.
Створимо спочатку Yo.jl:
</p>
<pre>
function yoyo()
  println(c"Hello")
end
</pre>
<p>
Далі запустимо консоль julia та зробимо з цієї функції yoyo бібліотеку (shared object).
</p>
<pre>
using StaticCompiler
using StaticTools
include("./Yo.jl")
compile_shlib(yoyo,(),"."
</pre>
<p>
А далі напишемо маленьку програму на C. Ми хочемо викликати функцію yoyo. У останнії версіях компілятора 
clang у зовнішніх іменах підкреслювання на початку ідентифікатора взагалі не використовують. Тому пищемо my.c 
ось так:
</p>
<pre>
#include <stdio.h>

extern void yoyo();

int main(int argc,char** argv) {
  yoyo();
  return 0;
}
</pre>
<p>
Оскільки yoyo є shared library , то воно під час виконання усе одно має причепити libjulia та все ініціалізувати. Тому нам у 
програмі на C більше нічого не потрібно, а тепер збираємо усе до купи:
</p> 
<pre>
clang ./my.c ./yoyo.so -s -o ./my
</pre>
<p>
Далі просто виконуємо ./my.  Ну там ще власне my вийде на 2 кілобайта меншою аніж yoyo , котре могло вийти по compile_executable
</p>



