<h1>MyJollyJulia</h1>
<h2>
це наш experience щодо Julia Programming Language - <a href="https://julialang.org/">jullialang.org</a>
</h2>
<p>
під Linux це ставиться по snap install --classic julia , але можна просто по іконці snap store клікнути.
</p>
<p>
Тут будуть деякі цікаві приклади використання - у тому числі в контексті ML. Дехто у Америці називає цю мову програмування Fast Python - але тут вони помиляються. Це справді компільована мова програмування. З програми на Julia справді можна викликати пакети python - у т.ч. TensorFlow. Але не тільки Python - можна C та csharp. Зрештою julia існує рідна імплементація tkinter та Gtk.  
</p>
<p>
Детальніше усе буде описуватися на Wiki Pages у цьому проекті 
<a href="https://gitlab.com/evilmike/myjollyjulia/-/wikis/home">ось тут</a>
</p>
